public class MethodsTest
{
	public static void main (String[] args)
	{
		int x = 5;
		//System.out.println(x);
		//methodNoInputNoReturn();
		//System.out.println(x);
		//methodOneInputNoReturn(x + 10);
		//System.out.println(x);
		//methodTwoInputNoReturn(5,5.55);
		//int z = methodNoInputReturnInt();
		//System.out.println(z);
		//double sqrtNum = sumSquareRoot(9,5);
		//System.out.println(sqrtNum);
		//String s1 = "java";
		//String s2 = "programming";
		//System.out.println(s1.length());
		//System.out.println(s2.length());
		SecondClass sc = new SecondClass();
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and return nothing.");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike)
	{
		System.out.println("Inside this method one input no return.");
		youCanCallThisWhateverYouLike -= 5;
		System.out.println(youCanCallThisWhateverYouLike);
	}
	
	public static void methodTwoInputNoReturn(int firstNum, double secondNum)
	{
		System.out.println(firstNum + "   " + secondNum);
	}
	
	public static int methodNoInputReturnInt()
	{
		return 5;
	}
	
	public static double sumSquareRoot(int num1, int num2)
	{
		double result = num1 + num2;
		result = Math.sqrt(result);
		return result;
	}
}