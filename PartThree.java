import java.util.Scanner;
public class PartThree
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter two numbers to go through the calculator.");
		double num1 = scan.nextDouble();
		System.out.println("Please enter the second number:");
		double num2 = scan.nextDouble();
		Calculator calc = new Calculator();
		System.out.println(Calculator.add(num1, num2) + "   (addition method)");
		System.out.println(Calculator.subtract(num1, num2) + "   (subtraction method)");
		System.out.println(calc.multiply(num1, num2) + "   (multiplication method)");
		System.out.println(calc.divide(num1, num2) + "   (division method)");
		
	}
}